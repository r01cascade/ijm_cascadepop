library(readxl)
library(foreign)
weights <- read.dbf("J252148.dbf")

###create a unique identifier for each individual 
weights$ID <- (weights$ER30001*1000) + weights$ER30002
weights$sampleornon <- weights$ER32006

##how many non-sample individuals?
length(subset(weights, sampleornon==0))
###in this dataset there are only 306 non sample people... 

familyweights <- c("V439","V1014","V1609","V2321","V2968","V3301","V3721","V4224","V5099",
                   "V5665","V6212","V6805","V7451","V8103","V8727","V9433","V11079","V12446","V13687","V14737",
                   "V16208","V17612","V18945","V20245","V21549","V23363","ER4162","ER7000B","ER9251","ER12084",
                   "ER16518","ER20394","ER24179","ER28078","ER41069","ER47012","ER52436",
                   "ER58257","ER65492")
##labels for family weights                    
familyyears <- 1968:2015
##without the years data not available for
toremove <- c("1998|2000|2002|2004|2006|2008|2010|2012|2014")
library(stringr)
familyyears <- str_replace_all(familyyears, toremove, "")

familyyears <- as.character(na.omit(as.integer(familyyears, na.rm=T)))

##extract these variables from raw weight data 
fam <- weights[, familyweights]

names(fam) <- familyyears
fam$ID <- weights$ID
fam$sampleornon <- weights$sampleornon

library(tidyr)
###change the data to long form
fam <- gather(fam, year, weight, "1968":"2015", factor_key=TRUE)
fam$year <- as.numeric(as.character(fam$year))
fam <- subset(fam, year==1979)

library(dplyr)
library(plyr)
###for each individual, what is the mean weight of non=zero weights 
meanperson <- ddply(fam,~ID,summarise,
                    meanweight=mean(weight, na.rm=T))

####final observation for each person 

fam <- na.omit(fam)

fam <- subset(fam, weight!=0)

fam$year  <- NULL

fam$uniqueID <- fam$ID

fam$ID <- NULL
fam$sampleornon <- NULL

microsimdata <- left_join(microsimdata, fam, by="uniqueID")
library(splitstackshape)

microsimdata <- na.omit(microsimdata)

microsimdata <- expandRows(microsimdata, "weight")


?expandRows

