# README #

This is the source code for population microsynthesis and microsimulation model by the CASCADE project for a publication in the International Journal of Microsimulation.

Publication title: Introducing CASCADEPOP: an open-source sociodemographic simulation platform for US health policy appraisal 

Authors: Alan Brennan, Charlotte Buckley, Tuong Manh Vu, Charlotte Probst, Alexandra Nielsen, Hao Bai, Thomas Broomhead, Thomas Greenfield, William Kerr, Petra S. Meier, Jurgen Rehm, Paul Shuper, Mark Strong, Robin C. Purshouse

This repo is hereby licensed for use under the GNU GPL version 3.

We are open to contribution via pull requests.


### What is this repository for? ###

This repository contains the source code (in R) for:

(1) Processing data for and generating a micro-synthetic population for the USA and US States

(2) Fitting a multi-state markov model to estimate transitions between social role combinations in the PSID data

(3) Implementing the microsimulation over time adding and removing individuals due to births, deaths and migration and adjusting social role transitions.

### How do I get set up? ###

To set up ensure you have R installed and the required packages. The packages required for each script are loaded at the start. In order for these to work they must already be installed i.e. using install.packages("packagename")

The source code reads in the raw data as downloaded from the source. All data sources are available online and must be downloaded into the correct folder (indicated in script) before executing scripts. 

The raw data can be found from the following sources: 

The National Survey on Drug Use and Health is publicly available and can be accessed from the Substance Abuse and Mental Health Data Archive https://datafiles.samhsa.gov/

 

The Panel Study of Income Dynamics is publicly available and can be accessed from the Institute for Social Research, University of Michigan https://simba.isr.umich.edu/data/data.aspx

 

U.S Census data is publicly available and can be accessed from IPUMS National Historic Geographic Information Service https://data2.nhgis.org/main

 

The American Community survey is publicly available and can be accessed from IPUMS-USA https://usa.ipums.org/usa/

 

Mortality data is publicly available and can be accessed from the Center for Disease Control and Prevention https://wonder.cdc.gov/mortSQL.html

### Who do I talk to? ###

Contact c.m.buckley@sheffield.ac.uk
