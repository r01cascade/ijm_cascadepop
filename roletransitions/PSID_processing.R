####PSID data processing for MSM modelling 
#Author: C Buckley c.m.buckley@sheffield.ac.uk

setwd("~/Desktop/repos/IJM_CASCADEPOP/roletransitions")
library(readxl)
library(stringr)
library(tidyverse)
library(plyr)
library(dplyr)

psid <- read_excel("J251816.xlsx", sheet=1)

varnames <- (1979:2015)

toremove <- c("1998|2000|2002|2004|2006|2008|2010|2012|2014")

varnames <- str_replace_all(varnames, toremove, "")

varnames <- as.character(na.omit(as.integer(varnames, na.rm=T)))

###interview nos 

interviewno <- c("ER30283","ER30313","ER30343","ER30373","ER30399","ER30429","ER30463","ER30498","ER30535","ER30570", "ER30606","ER30642",
                 "ER30689","ER30733","ER30806","ER33101","ER33201","ER33301","ER33401","ER33501","ER33601","ER33701","ER33801","ER33901",
                 "ER34001","ER34101","ER34201","ER34301")

intnos <- psid[, interviewno]

names(intnos) <- varnames

intnos$origINTNO <- psid$ER30001
intnos$ID <- psid$ER30002
intnos$uniqueID <- (intnos$origINTNO*1000) + intnos$ID
intnos$sex <- psid$ER32000
intnos$sex <- revalue(as.factor(intnos$sex), c("1"="male", "2"="female", "9"=NA))
summary(intnos$sex)

length(unique(intnos$uniqueID))

intnos <- gather(intnos, year, interviewno, "1979":"2015", factor_key=TRUE)
intnos$ID <- NULL
intnos$origINTNO <- NULL
intnos$year <- as.numeric(as.character(intnos$year))

##no of children vars 

children <- c("V6465","V7070","V7661","V8355","V8964","V10422","V11609","V13014","V14117","V15133",
              "V16634","V18052","V19352","V20654","ER2010","ER5009","ER7009","ER10012","ER13013","ER17016","ER21020","ER25020","ER36020",
              "ER42020","ER47320","ER53020","ER60021")
childvars <- psid[, children]

toremove <- c("1993|1998|2000|2002|2004|2006|2008|2010|2012|2014")
varnameschild <- str_replace_all(varnames, toremove, "")
varnameschild <- as.character(na.omit(as.integer(varnameschild, na.rm=T)))
names(childvars) <- varnameschild
childvars$origINTNO <- psid$ER30001
childvars$ID <- psid$ER30002
childvars$uniqueID <- (childvars$origINTNO*1000) + childvars$ID
childvars <- gather(childvars, year, no_ofchildren, "1979":"2015", factor_key=TRUE)
childvars$origINTNO <- NULL
childvars$ID <- NULL
childvars$year <- as.numeric(as.character(childvars$year))

##relation to householder vars 

relation <- c("ER30285","ER30315","ER30345","ER30375","ER30401","ER30431","ER30465","ER30500","ER30537","ER30572","ER30608",
              "ER30644","ER30691","ER30735","ER30808","ER33103","ER33203","ER33303","ER33403","ER33503","ER33603","ER33703",
              "ER33803","ER33903","ER34003","ER34103","ER34203","ER34303")

relationvars <- psid[, relation]
names(relationvars) <- varnames

relationvars$origINTNO <- psid$ER30001
relationvars$ID <- psid$ER30002
relationvars$uniqueID <- (relationvars$origINTNO*1000) + relationvars$ID
relationvars <- gather(relationvars, year, relation_householder, "1979":"2015", factor_key=TRUE)
relationvars$origINTNO <- NULL
relationvars$ID <- NULL
relationvars$year <- as.numeric(as.character(relationvars$year))

df <- merge(intnos, childvars, all=T)
df <- merge(df, relationvars, all=T)
df <- df%>% drop_na(no_ofchildren)

##read in the employment vars 

employment <- c("ER30441","ER30474","ER30509","ER30545","ER30744","ER30580","ER30616","ER30653","ER30699","ER30382","ER30411","ER30816",
                "ER33111","ER33211","ER33311","ER33411","ER33512","ER33612","ER33712","ER33813","ER33913","ER34016","ER34116","ER34216","ER34317")
empvars <- psid[, employment]

names(empvars) <- varnames
empvars$origINTNO <- psid$ER30001
empvars$ID <- psid$ER30002
empvars$uniqueID <- (empvars$origINTNO*1000) + empvars$ID
empvars <- gather(empvars, year, employment, "1979":"2015", factor_key=TRUE)
empvars$origINTNO <- NULL
empvars$ID <- NULL
empvars$year <- as.numeric(as.character(empvars$year))

df <- merge(df, empvars, all=T)
df <- df%>% drop_na(employment)

###recoding employment variables 
df$employment <- revalue(as.factor(df$employment), c("0" = NA, "1"="employed", "2"="unemployed","3"="unemployed","4"="retired",
                                                               "5"="disabled", "6"="housewife", "7"="student", "8"="other","9"=NA))

df$employmentbin <- revalue(df$employment, c("employed"=1, "unemployed"=0, "retired"=0, "disabled"=0, "housewife"=0, "student"=0,
                                                       "other"=0))

df$employmentbin <- as.numeric(as.character(df$employmentbin))

df <- df%>% drop_na(employment)

##age variables 

agevars <- c("ER30286","ER30316","ER30346","ER30376","ER30402","ER30432","ER30466","ER30501","ER30538","ER30573","ER30609","ER30645","ER30692",
             "ER30736", "ER30809","ER33104","ER33204","ER33304","ER33404","ER33504","ER33604","ER33704","ER33804","ER33904","ER34004","ER34104","ER34204",
             "ER34305")

age <- psid[, agevars]
names(age) <- varnames
age$origINTNO <- psid$ER30001
age$ID <- psid$ER30002
age$uniqueID <- (age$origINTNO*1000) + age$ID
age <- gather(age, year, age, "1979":"2015", factor_key=TRUE)

age$origINTNO <- NULL
age$ID <- NULL
age$year <- as.numeric(as.character(age$year))


df <- merge(df, age, all=T)

###read in the marriage data 
marriage <- read_excel("J251726.xlsx", sheet=1)
marriage <- marriage[, -c(1)]
names(marriage) <- c("origINTNO", "ID", "year_first_began", "year_first_ended", "year_recent_began", "year_recent_ended")
marriage$uniqueID <- (marriage$origINTNO*1000) + marriage$ID
no_marriages <- data.frame(psid$ER32034, psid$ER30001, psid$ER30002)
names(no_marriages) <- c("no_marriages", "origINTNO", "ID")
marriage <- merge(no_marriages, marriage, all=T)

##coding if ever married
marriage$nevermarried <- ifelse(marriage$no_marriages==0, 1, 0)

###create a dataframe to merge the marriage records with 
newdata <- data.frame(df$uniqueID, df$year)
names(newdata) <- c("uniqueID", "year")

marriage <- merge(newdata, marriage, all=T)
marriage$year <- as.numeric(as.character(marriage$year))

marriage<- subset(marriage, no_marriages!=99)

marriage$married1 <- ifelse(marriage$year>= marriage$year_first_began & marriage$year<=marriage$year_first_ended, 1, 0)

marriage$married2 <- ifelse(marriage$year>=marriage$year_recent_began & marriage$year <= marriage$year_recent_ended, 1, 0)

marriage$married <- ifelse(marriage$married1 ==1 | marriage$married2 == 1, 1, 0)

##checking - this subset should be 0 - no one has 0 marriages and is coded as being married at any point 
subset(marriage, no_marriages==0 & married==1)

##tidy up marriage dataset 
marriage <- data.frame(marriage$uniqueID, marriage$year, marriage$no_marriages, marriage$married)
names(marriage) <- c("uniqueID", "year", "no_marriages", "married")

marriage <- marriage %>% drop_na(married)


##merging back with the main dataframe 
df <- merge(df, marriage, all=T)

##tidying up 
rm(age, childvars, empvars, intnos, marriage, newdata, no_marriages, psid, relationvars, agevars, children, employment,
   interviewno, relation, toremove, varnameschild)

gc()

##getting year of entry 

year <- read.csv("agedata.csv")
year <- data.frame(year$uniqueID, year$year_entry)
names(year) <- c("uniqueID", "year_entry")
year$uniqueID <- as.numeric(as.character(year$uniqueID))
year <- year %>% distinct

gc()

###merging together

df <- merge(df, year, all=T)

rm(year)

gc()

###GETTING the data ready for MSM 

##removing all duplicated rows 

df$firstobs <- NA

###rename all year_entrys before 1979 as 1979 so we can derive first obs for this model
df$year_entry
df$newyear <- ifelse(df$year_entry < 1979, 1979, df$year_entry)
summary(df$newyear)
df$firstobs <- ifelse(df$year==df$newyear, 1, 0)

##get rid of under 12s and missing ages

df <- subset(df, age >= 12)
df <- subset(df, age!=999)

##checking
length(unique(df$uniqueID))
length(unique(df$year))

##creating age categories

df$agecat <- cut(df$age,
                   breaks=c(11,17,24,39,64,120),
                   labels=c("12-17","18-24","25-39", "40-64", "65+"))

###categorising parents and non parents 

##first code if people are a householder or spouse = householder (for deriving parenting)
df$householder <- ifelse(df$relation_householder==1 | df$relation_householder==2 | df$relation_householder==9 | df$relation_householder==10 | df$relation_householder==20|
                             df$relation_householder==22 | df$relation_householder==90, 1, 0)

##parent = 1 if they are householder or spouse/live in partner and there are any no. of children in the household 
df$parent <- ifelse(df$householder == 1 & df$no_ofchildren > 0, 1, 0)

###remove NAS for vars of interest

summary(df$parent)
df <- df%>% drop_na(parent)

summary(df$married)
df <- df%>% drop_na(married)

summary(df$employmentbin)

df <- subset(df, sex!=9)

# ####adding weights to the df
# ####reweighting the survey data 
# 
# weights <- read_excel("J251820.xlsx", sheet=1)
# 
# combindividualweights <- c("ER30312", "ER30342", "ER30372", "ER30398","ER30428","ER30462","ER30497","ER30534",
#                            "ER30569","ER30605","ER30641","ER30688","ER30732","ER30805","ER30864","ER33119",
#                            "ER33275","ER33318","ER33430", "ER33546", "ER33637", "ER33740","ER33848","ER33950",
#                            "ER34045","ER34154","ER34268","ER34413")
# 
# weights1 <- weights[, combindividualweights]
# names(weights1) <- varnames
# 
# weights1$origINTNO <- weights$ER30001
# weights1$ID <- weights$ER30002
# weights1$uniqueID <- (weights1$origINTNO*1000) + weights1$ID
# 
# weights1$origINTNO <- NULL
# weights1$ID <- NULL
# 
# weights1 <- gather(weights1, year, weight, "1979":"2015", factor_key=TRUE)
# weights1$year <- as.numeric(as.character(weights1$year))
# 
# msm <- merge(df, weights1, all=T)

msm <- df
rm(weights, weights1, combindividualweights, varnames)

msm$X <- NULL

msm <- msm%>% drop_na(sex)
msm <- msm%>% drop_na(age)
msm <- msm%>% drop_na(married)
msm <- msm%>% drop_na(parent)
msm <- msm%>% drop_na(employmentbin)

msm <- msm %>% distinct

####43258 individuals 
length(unique(msm$uniqueID))

###create a dataframe for MSM modelling 

msmdata <- data.frame(msm$uniqueID, msm$year, msm$sex, msm$agecat, msm$age, msm$married, msm$parent, msm$employmentbin, msm$firstobs)
names(msmdata) <- c("ID", "year", "sex", "agecat", "age", "married", "parent", "employed", "firstobs")

write.csv(msmdata, "processed_psid.csv")

rm(df, msm)
