####script for extracting all transition probabilities for the models ####

library(msm)
library(reshape2)
library(tidyverse)
library(stringr)
library(plyr)
library(dplyr)
options(scipen=999)
setwd("~/Desktop/IJM_CASCADEPOP/roletransitions")

msm1 <- read.csv("msmdata.csv")

###get mapping for ages
ages <- data.frame(msm1$age, msm1$agecennew, msm1$agesqcennew)
ages <- unique(ages)                   
names(ages) <- c("age", "agecen", "agesqcen")

age <- ddply(ages,~age,summarise,
             agecen=max(agecen),
             agesqcen=max(agesqcen))
age <- subset(age, age<=80)

agecen <- age$agecen
agesqcen <- age$agesqcen

age <- data.frame(age=18:80, agecen=agecen, agesqcen=agesqcen)

model1 <- readRDS("psidmsm1.1.RDS")
model2 <- readRDS("psidmsm2.1.RDS")
model3 <- readRDS("psidmsm3.1.RDS")
model4 <- readRDS("psidmsm4.1.RDS")
model5 <- readRDS("psidmsm5.1.RDS")
model1list <- list(model1, model2, model3, model4, model5)

outputF <- list()
outputM <- list()

for(i in 1:length(agecen)){
   outputF[[i]] <- 
     lapply(model1list, function(x){print(pmatrix.msm(x, covariates=list(sex=0, agecennew=i)))})}
for(i in 1:length(agecen)){
  outputM[[i]] <- 
    lapply(model1list, function(x){print(pmatrix.msm(x, covariates=list(sex=1, agecennew=i)))})}

outputF <- lapply(outputF, function(x){data.frame(do.call(rbind, x))})
outputF2 <- data.frame(do.call(rbind, outputF))
outputF2$yeargroup <- rep(1:5, each=8, times=62)
outputF2$statefrom <- rep(1:8)
outputF2$age <- rep(agecen, each=40)
outputF2 <- gather(outputF2, stateto, TP, State.1:State.8)
outputF2$stateto <-  as.integer(sub("State.", "", outputF2$stateto))


outputM <- lapply(outputM, function(x){data.frame(do.call(rbind, x))})
outputM2 <- data.frame(do.call(rbind, outputM))
outputM2$yeargroup <- rep(1:5, each=8, times=62)
outputM2$statefrom <- rep(1:8)
outputM2$age <- rep(agecen, each=40)
outputM2 <- gather(outputM2, stateto, TP, State.1:State.8)
outputM2$stateto <-  as.integer(sub("State.", "", outputM2$stateto))


